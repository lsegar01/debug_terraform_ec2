#!/bin/bash

terraform plan -out=$PLAN
terraform show --json $PLAN | convert_report > $JSON_PLAN_FILE
