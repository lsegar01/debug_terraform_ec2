# README

> Debug [tf_infra_ec2](https://gitlab.com/act5/sdlc/tfinfra_ec2) CI/CD WF


## Build

```bash
docker build -t {image_name}:{tag} .
```

## Run

```bash
docker run -v /{path/to/tfinfra_ec2/main.tf}:/main.tf -e "AWS_DEFAULT_REGION={aws_def_reg}" -e "AWS_SECRET_ACCESS_KEY={aws_sec_acc_key}" -e "AWS_ACCESS_KEY_ID={aws_acc_key_id}" -e "PLAN=plan.tfplan" -e "JSON_PLAN_FILE=tfplan.json" {image_name}:{tag} 
```

