FROM alpine:20200917

COPY --from=hashicorp/terraform:light /bin/terraform /bin/

RUN apk add --no-cache bash
RUN apk add --no-cache jq
RUN apk add --no-cache \
        python3 \
        py3-pip \
    && pip3 install --upgrade pip \
    && pip3 install \
        awscli \
    && rm -rf /var/cache/apk/*

# 2test gitlab ci-cd
RUN mkdir /ci_cd
COPY ci_* /ci_cd/
RUN chmod u+x /ci_cd/*
COPY convert_report /bin/
RUN chmod u+x /bin/convert_report
CMD ["/ci_cd/ci_workflow.sh"]
